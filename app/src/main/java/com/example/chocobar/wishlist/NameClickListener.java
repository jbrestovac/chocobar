package com.example.chocobar.wishlist;

public interface NameClickListener {

    void onNameClick(int position);
}
