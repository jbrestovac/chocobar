package com.example.chocobar.wishlist;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.chocobar.R;

import java.util.ArrayList;
import java.util.List;

public class WishListActivity extends AppCompatActivity implements NameClickListener {

    private RecyclerView recyclerView;
    private RecyclerAdapter recyclerAdapter;
    private Button buttonAdd;

    private int lastPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        buttonAdd = findViewById(R.id.btnAdd);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createInputDialog();
            }
        });
        setupRecycler();
    }

    private void createInputDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(WishListActivity.this);
        alertDialog.setTitle("Nova želja");
        alertDialog.setMessage("Unesite novu želju:");

        final EditText input = new EditText(WishListActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("Unesi",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String newWish = input.getText().toString();
                        recyclerAdapter.addNewCell(newWish, 0);
                    }
                });

        alertDialog.setNegativeButton("Odustani",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    private void setupRecycler() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerAdapter = new RecyclerAdapter(this);
        recyclerView.setAdapter(recyclerAdapter);
    }

    public void addCell(View view){
        recyclerAdapter.addNewCell("petra", 1);
    }

    public void removeCell(View view){
        recyclerAdapter.removeCell(lastPosition);
    }

    @Override
    public void onNameClick(int position) {
        lastPosition = position;
        Toast.makeText(this, "Izabrana želja: " + recyclerAdapter.getItem(position), Toast.LENGTH_SHORT).show();
    }
}