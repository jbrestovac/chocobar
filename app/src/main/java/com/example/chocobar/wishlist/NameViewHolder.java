package com.example.chocobar.wishlist;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chocobar.R;

public class NameViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView textViewCell01;
    private NameClickListener nameClickListener;

    public NameViewHolder(@NonNull View itemView, NameClickListener listener) {
        super(itemView);
        textViewCell01 = itemView.findViewById(R.id.textViewCell01);
        this.nameClickListener = listener;
        itemView.setOnClickListener(this);
    }

    public void setName(final String name){
        textViewCell01.setText(name);
    }

    @Override
    public void onClick(View v) {
        nameClickListener.onNameClick(getAdapterPosition());
    }
}
