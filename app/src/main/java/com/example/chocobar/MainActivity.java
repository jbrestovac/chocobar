package com.example.chocobar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.chocobar.cake.CakeActivity;
import com.example.chocobar.icecream.IceCreamActivity;
import com.example.chocobar.wishlist.WishListActivity;

public class MainActivity extends AppCompatActivity {

    private Button button01;
    private Button button02;
    private Button button03;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupButtons();
    }

    private void setupButtons() {
        button01=(Button) findViewById(R.id.button);
        button01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCake();
            }

        });

        button02= (Button) findViewById(R.id.button2);
        button02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openIceCream();
            }
        });

        button03=(Button) findViewById(R.id.button3);
        button03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWishList();
            }
        });
    }

    public void openCake(){
        final Intent intent =new Intent(this, CakeActivity.class);
        startActivity(intent);
    }

    public void openIceCream(){
        final Intent intent= new Intent(this, IceCreamActivity.class);
        startActivity(intent);
    }

    public void openWishList(){
        final Intent intent = new Intent(this, WishListActivity.class);
        startActivity(intent);
    }


}