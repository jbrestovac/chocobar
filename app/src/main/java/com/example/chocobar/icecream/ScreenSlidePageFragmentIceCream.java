package com.example.chocobar.icecream;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.chocobar.R;

public class ScreenSlidePageFragmentIceCream extends Fragment {
    // tab01
    private static TextView tab01TextView01;
    private static TextView tab01TextView02;
    private static  ImageView tab01ImageView01;

    // tab02
    private static TextView tab02TextView01;
    private static TextView tab02TextView02;
    private static ImageView tab02ImageView01;
    private static RadioButton tab02RadioIceCream01;
    private static RadioButton tab02RadioIceCream02;
    private static RadioButton tab02RadioIceCream03;
    private static EditText tab02InputSliceNumber;
    private static Button tab02CalculationButton;

    public static ScreenSlidePageFragmentIceCream newInstance() {
        return new ScreenSlidePageFragmentIceCream();
    }

    public static void hideTab1Values() {
            tab01TextView01.setVisibility(View.GONE);
            tab01TextView02.setVisibility(View.GONE);
            tab01ImageView01.setVisibility(View.GONE);
            showTab2Values();
    }

    private static void showTab2Values() {
        tab02ImageView01.setVisibility(View.VISIBLE);
        tab02RadioIceCream01.setVisibility(View.VISIBLE);
        tab02RadioIceCream02.setVisibility(View.VISIBLE);
        tab02RadioIceCream03.setVisibility(View.VISIBLE);
        tab02TextView01.setVisibility(View.VISIBLE);
        tab02InputSliceNumber.setVisibility(View.VISIBLE);
        tab02TextView02.setVisibility(View.VISIBLE);
        tab02CalculationButton.setVisibility(View.VISIBLE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup
            container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_screen_slide_page, container, false);

        setupTab01(rootView);

       setupTab02(rootView);

        return rootView;
    }

    private void setupTab01(View rootView) {
        tab01TextView01 = rootView.findViewById(R.id.textView01);
        tab01TextView01.setText("Sladoledi");
        tab01TextView01.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tab01TextView01.setGravity(0);

        tab01TextView02 = rootView.findViewById(R.id.textView02);
        tab01TextView02.setText("\n\n" +
                "Sladoled je djelomično ili potpuno zamrznuta slastica koja se većinom sastoji od mlijeka ili mliječnih proizvoda (mlijeko u prahu, ugušćeno mlijeko, vrhnje ili maslac), te nemliječnih sastojaka koji se dodaju u smjesu u svrhu podešavanja okusa, arome, boje, mirisa i konzistencije (šećeri i zaslađivači, stabilizatori i emulgatori, uz mogući dodatak voća, voćnih aroma, jaja, čokolade ili nekih drugih dodataka).\n\nOsim toga, sladoled sadrži vodu i uklopljen zrak. Uklopljeni zrak služi povećanju volumena sladoleda, boljoj konzistenciji, te smanjuje osjećaj hladnoće u ustima tijekom konzumiranja.\n" +
                "\n" +
                "Sladoledi koje nudimo su:\n" +
                "Jagoda, vanilija i čokolada- svaki 150 kalorija/kugla");

        tab01ImageView01 = (ImageView)rootView.findViewById(R.id.imageView01);
        tab01ImageView01.setImageResource(R.drawable.sladoledi_slika);
    }


    private void setupTab02(View rootView) {
        tab02TextView01 = rootView.findViewById(R.id.textView05);
        tab02TextView01.setText("\nKoji ste sladoled pojeli?\n");
        tab02TextView01.setVisibility(View.GONE);

        tab02TextView02 = rootView.findViewById(R.id.textView06);
        tab02TextView02.setText("\nREZULTAT: 0\n");
        tab02TextView02.setVisibility(View.GONE);

        tab02ImageView01 = (ImageView)rootView.findViewById(R.id.imageView04);
        tab02ImageView01.setImageResource(R.drawable.slika_sladoled_izracun);
        tab02ImageView01.setVisibility(View.GONE);

        tab02InputSliceNumber = rootView.findViewById(R.id.inputSliceNumber01);
        tab02InputSliceNumber.setVisibility(View.GONE);
        tab02InputSliceNumber.setHint("Koliko kugli ste pojeli?");
        tab02InputSliceNumber.setFocusable(true);
        tab02InputSliceNumber.requestFocus();

        tab02RadioIceCream01 = rootView.findViewById(R.id.radioCake01);
        tab02RadioIceCream01.setVisibility(View.GONE);
        tab02RadioIceCream01.setText("Jagoda");

        tab02RadioIceCream02 = rootView.findViewById(R.id.radioCake02);
        tab02RadioIceCream02.setVisibility(View.GONE);
        tab02RadioIceCream02.setText("Vanilija");

        tab02RadioIceCream03 = rootView.findViewById(R.id.radioCake03);
        tab02RadioIceCream03.setVisibility(View.GONE);
        tab02RadioIceCream03.setText("Čokolada");

        tab02CalculationButton = rootView.findViewById(R.id.calculationButton01);
        tab02CalculationButton.setText("IZRAČUN");
        tab02CalculationButton.setVisibility(View.GONE);
        tab02CalculationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!tab02RadioIceCream01.isChecked() && !tab02RadioIceCream02.isChecked() && !tab02RadioIceCream03.isChecked()){
                    Toast.makeText(getActivity(), "Unesite koji ste sladoled ste pojeli!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(tab02InputSliceNumber.getText().toString().trim().equals("")){
                    Toast.makeText(getActivity(), "Unesite koliko kugli ste pojeli!", Toast.LENGTH_SHORT).show();
                    return;
                }
                final int value = Integer.valueOf(tab02InputSliceNumber.getText().toString());
                if(tab02RadioIceCream01.isChecked()){
                    final int newValue = value * 150;
                    tab02TextView02.setText("\nREZULTAT: " + newValue + "\n");
                } else if(tab02RadioIceCream02.isChecked()){
                    final int newValue = value * 150;
                    tab02TextView02.setText("\nREZULTAT: " + newValue+ "\n");
                } else if(tab02RadioIceCream03.isChecked()){
                    final int newValue = value * 150;
                    tab02TextView02.setText("\nREZULTAT: " + newValue+ "\n");
                }
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

}