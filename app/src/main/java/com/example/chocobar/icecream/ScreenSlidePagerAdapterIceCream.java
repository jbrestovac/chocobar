package com.example.chocobar.icecream;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.chocobar.cake.ScreenSlidePageFragmentCake;

import java.util.Locale;

public class ScreenSlidePagerAdapterIceCream extends FragmentStatePagerAdapter {
    private static final int NUM_PAGES = 2;
    private static final String INFORMATION_BASE_NAME = "INFORMACIJE";
    private static final String CALCULCATION_BASE_NAME = "IZRAČUN";
    public ScreenSlidePagerAdapterIceCream(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return ScreenSlidePageFragmentIceCream.newInstance();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0){
            return String.format(Locale.getDefault(), INFORMATION_BASE_NAME, position + 1);
        }
        return String.format(Locale.getDefault(), CALCULCATION_BASE_NAME, position + 1);
    }
    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
