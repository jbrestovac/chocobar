package com.example.chocobar.icecream;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.chocobar.R;
import com.google.android.material.tabs.TabLayout;

public class IceCreamActivity extends AppCompatActivity {
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ice_cream);
        initViews();
        setUpPager();
    }
    private void initViews() {
        mViewPager = findViewById(R.id.viewPager);
        mTabLayout = findViewById(R.id.tab);
    }
    private void setUpPager() {
        PagerAdapter pagerAdapter = new ScreenSlidePagerAdapterIceCream(getSupportFragmentManager());
        mViewPager.setAdapter(pagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                if(position == 1){
                    ScreenSlidePageFragmentIceCream.hideTab1Values();
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

}
