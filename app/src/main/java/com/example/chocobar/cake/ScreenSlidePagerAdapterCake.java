package com.example.chocobar.cake;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.Locale;

public class ScreenSlidePagerAdapterCake extends FragmentStatePagerAdapter {
    private static final int NUM_PAGES = 2;
    private static final String INFORMATION_BASE_NAME = "INFORMACIJE";
    private static final String CALCULCATION_BASE_NAME = "IZRAČUN";
    public ScreenSlidePagerAdapterCake(FragmentManager fm) {
        super(fm);
    }
    @Override
    public Fragment getItem(int position) {
        return ScreenSlidePageFragmentCake.newInstance();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0){
            return String.format(Locale.getDefault(), INFORMATION_BASE_NAME, position + 1);
        }
        return String.format(Locale.getDefault(), CALCULCATION_BASE_NAME, position + 1);
    }
    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
