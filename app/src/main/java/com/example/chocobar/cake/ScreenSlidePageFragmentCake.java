package com.example.chocobar.cake;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.chocobar.R;

public class ScreenSlidePageFragmentCake extends Fragment {
    // tab01
    private static TextView tab01TextView01;
    private static TextView tab01TextView02;
    private static TextView tab01TextView03;
    private static TextView tab01TextView04;
    private static ImageView tab01ImageView01;
    private static ImageView tab01ImageView02;
    private static ImageView tab01ImageView03;

    // tab02
    private static TextView tab02TextView01;
    private static TextView tab02TextView02;
    private static ImageView tab02ImageView01;
    private static RadioButton tab02RadioCake01;
    private static RadioButton tab02RadioCake02;
    private static RadioButton tab02RadioCake03;
    private static EditText tab02InputSliceNumber;
    private static Button tab02CalculationButton;

    public static ScreenSlidePageFragmentCake newInstance() {
        return new ScreenSlidePageFragmentCake();
    }

    public static void hideTab1Values() {
            tab01TextView01.setVisibility(View.GONE);
            tab01TextView02.setVisibility(View.GONE);
            tab01TextView03.setVisibility(View.GONE);
            tab01TextView04.setVisibility(View.GONE);
            tab01ImageView01.setVisibility(View.GONE);
            tab01ImageView02.setVisibility(View.GONE);
            tab01ImageView03.setVisibility(View.GONE);
            showTab2Values();
    }

    private static void showTab2Values() {
        tab02ImageView01.setVisibility(View.VISIBLE);
        tab02RadioCake01.setVisibility(View.VISIBLE);
        tab02RadioCake02.setVisibility(View.VISIBLE);
        tab02RadioCake03.setVisibility(View.VISIBLE);
        tab02TextView01.setVisibility(View.VISIBLE);
        tab02InputSliceNumber.setVisibility(View.VISIBLE);
        tab02TextView02.setVisibility(View.VISIBLE);
        tab02CalculationButton.setVisibility(View.VISIBLE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup
            container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_screen_slide_page, container, false);
        setupTab01(rootView);
        setupTab02(rootView);
        return rootView;
    }

    private void setupTab01(View rootView) {
        tab01TextView01 = rootView.findViewById(R.id.textView01);
        tab01TextView01.setText("Torte i kolači");
        tab01TextView01.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tab01TextView01.setGravity(0);

        tab01TextView02 = rootView.findViewById(R.id.textView02);
        tab01TextView02.setText("\n\n" +
                "Rođendanske ili vjenčane, voćne ili čokoladne, kremaste ili čvrste. Torte su uvijek tu uz tebe, za svaku prirodu i u svakakvim kombinacijama okusa.\n" +
                "\nNudimo 3 vrste torti: Blitz torta, Doboš torta, Schwarzwald torta.\n\n\nBlitz torta: 200 kalorija/komad");

        tab01TextView03 = rootView.findViewById(R.id.textView03);
        tab01TextView03.setText("\nDoboš torta: 300 kalorija/komad");

        tab01TextView04 = rootView.findViewById(R.id.textView04);
        tab01TextView04.setText("\nSchwarzwald torta: 400 kalorija/komad");

        tab01ImageView01 = (ImageView)rootView.findViewById(R.id.imageView01);
        tab01ImageView01.setImageResource(R.drawable.blitz_torta);

        tab01ImageView02 = (ImageView)rootView.findViewById(R.id.imageView02);
        tab01ImageView02.setImageResource(R.drawable.dobos_torta);

        tab01ImageView03 = (ImageView)rootView.findViewById(R.id.imageView03);
        tab01ImageView03.setImageResource(R.drawable.schwarzward_torta);
    }

    private void setupTab02(View rootView) {
        tab02TextView01 = rootView.findViewById(R.id.textView05);
        tab02TextView01.setText("\nKoji ste kolač pojeli?\n");
        tab02TextView01.setVisibility(View.GONE);

        tab02TextView02 = rootView.findViewById(R.id.textView06);
        tab02TextView02.setText("\nREZULTAT: 0\n");
        tab02TextView02.setVisibility(View.GONE);

        tab02ImageView01 = (ImageView)rootView.findViewById(R.id.imageView04);
        tab02ImageView01.setImageResource(R.drawable.torte_izracun);
        tab02ImageView01.setVisibility(View.GONE);

        tab02InputSliceNumber = rootView.findViewById(R.id.inputSliceNumber01);
        tab02InputSliceNumber.setVisibility(View.GONE);
        tab02InputSliceNumber.setHint("Koliko komada ste pojeli?");
        tab02InputSliceNumber.setFocusable(true);
        tab02InputSliceNumber.requestFocus();

        tab02RadioCake01 = rootView.findViewById(R.id.radioCake01);
        tab02RadioCake01.setVisibility(View.GONE);
        tab02RadioCake01.setText("Blitz");

        tab02RadioCake02 = rootView.findViewById(R.id.radioCake02);
        tab02RadioCake02.setVisibility(View.GONE);
        tab02RadioCake02.setText("Doboš");

        tab02RadioCake03 = rootView.findViewById(R.id.radioCake03);
        tab02RadioCake03.setVisibility(View.GONE);
        tab02RadioCake03.setText("Schwarzwald");

        tab02CalculationButton = rootView.findViewById(R.id.calculationButton01);
        tab02CalculationButton.setText("IZRAČUN");
        tab02CalculationButton.setVisibility(View.GONE);
        tab02CalculationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!tab02RadioCake01.isChecked() && !tab02RadioCake02.isChecked() && !tab02RadioCake03.isChecked()){
                    Toast.makeText(getActivity(), "Unesite koji ste kolac ste pojeli!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(tab02InputSliceNumber.getText().toString().trim().equals("")){
                    Toast.makeText(getActivity(), "Unesite koliko komada ste pojeli!", Toast.LENGTH_SHORT).show();
                    return;
                }
                final int value = Integer.valueOf(tab02InputSliceNumber.getText().toString());
                if(tab02RadioCake01.isChecked()){
                    final int newValue = value * 200;
                    tab02TextView02.setText("\nREZULTAT: " + newValue + "\n");
                } else if(tab02RadioCake02.isChecked()){
                    final int newValue = value * 300;
                    tab02TextView02.setText("\nREZULTAT: " + newValue+ "\n");
                } else if(tab02RadioCake03.isChecked()){
                    final int newValue = value * 400;
                    tab02TextView02.setText("\nREZULTAT: " + newValue+ "\n");
                }
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

}